const Encore = require('@symfony/webpack-encore');
const glob = require('glob');

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    // .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .addEntry('app', glob.sync('./assets/**/*.js'))
    .enableSassLoader()
    .enableBuildNotifications()
    .enableStylusLoader()
    // .configureFilenames()
    .configureBabel(function (babelConfig) {
        babelConfig.presets.push('es2017');
        babelConfig.compact = true;
    })
    .enableVueLoader()
;

module.exports = Encore.getWebpackConfig();
