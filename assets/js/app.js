import Vue from 'vue';
import axios from 'axios';
import vueAxios from 'vue-axios';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

import App from './vue/App';
import localeRu from 'element-ui/lib/locale/lang/ru-RU';
import LoginForm from './vue/element/LoginForm';
import ItemsList from './vue/element/ItemsList';
import ItemEditor from './vue/element/ItemEditor';
import Card from './vue/element/Card';
import Item from './vue/element/Item';
import Cabinet from './vue/element/Cabinet';
import Settings from "./vue/element/Settings";

require('../css/_reset.css');
require('../fonts/GooSans.ttf');
require('../css/app.styl');

Vue.component('App', App);
Vue.use(VueRouter);
Vue.use(vueAxios, axios);
Vue.use(Vuex);

Vue.use(ElementUI, {locale: localeRu});
Vue.use(LoginForm);
Vue.use(Card);

// Vue.config.devtools = false;
// Vue.config.debug = false;
// Vue.config.silent = true;

const router = new VueRouter({
    mode: 'history',
    routes: [
        {path: '/', component: LoginForm, name: 'LoginForm'},
        {path: '/list/', component: ItemsList, name: 'ItemsList'},
        {path: '/list/:itemId/', component: Item, name: 'Item'},
        {path: '/edit/:itemId/', component: ItemEditor, name: 'EditItem'},
        {path: '/create/', component: ItemEditor, name: 'CreateItem'},
        {path: '/cabinet/', component: Cabinet, name: 'Cabinet'},
        {path: '/settings/', component: Settings, name: 'Settings'},
    ],
});

const store = new Vuex.Store({
    state: {
        isAuth: false,
        user: {},
        itemsList: {},
        path: router.currentRoute.path,
        api: {},
        pagination: {},
        options: {},
        filters: {},
        isLoaded: false,
        query: {}
    },
    mutations: {
        set(state, {type, value}) {
            state[type] = value;
        },
        go(state, {path}) {
            router.push(path);
            state.path = path;
        },
        multiSet(state, {value}) {
            for (let key in value) {
                state[key] = value[key];
            }
        },
        endLoad(state) {
            state.isLoaded = true;
        },
        startLoad(state) {
            state.isLoaded = false;
        },
        deleteListItem(state, index) {
            state.itemsList.splice(index, 1);
        }
    },
    getters: {
        getPath: state => {
            state.path = router.currentRoute.path;
            return state.path;
        },
    },
});

const vm = new Vue({
    el: '#app',
    router,
    store,
    created: () => {
        axios.get('/api/init/').then(response => {
            const {data} = response;
            store.commit('multiSet', {value: data});
            store.commit('endLoad');
        });
    },
    watch: {
        '$route'() {

        }
    }
});

