<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = $allowSchemes = array();
        if ($ret = $this->doMatch($pathinfo, $allow, $allowSchemes)) {
            return $ret;
        }
        if ($allow) {
            throw new MethodNotAllowedException(array_keys($allow));
        }
        if (!in_array($this->context->getMethod(), array('HEAD', 'GET'), true)) {
            // no-op
        } elseif ($allowSchemes) {
            redirect_scheme:
            $scheme = $this->context->getScheme();
            $this->context->setScheme(key($allowSchemes));
            try {
                if ($ret = $this->doMatch($pathinfo)) {
                    return $this->redirect($pathinfo, $ret['_route'], $this->context->getScheme()) + $ret;
                }
            } finally {
                $this->context->setScheme($scheme);
            }
        } elseif ('/' !== $pathinfo) {
            $pathinfo = '/' !== $pathinfo[-1] ? $pathinfo.'/' : substr($pathinfo, 0, -1);
            if ($ret = $this->doMatch($pathinfo, $allow, $allowSchemes)) {
                return $this->redirect($pathinfo, $ret['_route']) + $ret;
            }
            if ($allowSchemes) {
                goto redirect_scheme;
            }
        }

        throw new ResourceNotFoundException();
    }

    private function doMatch(string $rawPathinfo, array &$allow = array(), array &$allowSchemes = array()): ?array
    {
        $allow = $allowSchemes = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $context = $this->context;
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        switch ($pathinfo) {
            default:
                $routes = array(
                    '/api/init/' => array(array('_route' => 'init', '_controller' => 'App\\Controller\\ApiController::initApp'), null, array('GET' => 0), null),
                    '/api/item/' => array(array('_route' => 'itemCreate', '_controller' => 'App\\Controller\\ApiController::createItem'), null, array('POST' => 0), null),
                    '/api/gallery/' => array(array('_route' => 'galleryCreate', '_controller' => 'App\\Controller\\ApiController::createGallery'), null, array('POST' => 0), null),
                    '/api/countries/' => array(array('_route' => 'allCountries', '_controller' => 'App\\Controller\\ApiController::getAllCountries'), null, array('GET' => 0), null),
                    '/api/items/' => array(array('_route' => 'items', '_controller' => 'App\\Controller\\ApiController::getItems'), null, array('GET' => 0), null),
                    '/api/options/' => array(array('_route' => 'createOptions', '_controller' => 'App\\Controller\\ApiController::createOptions'), null, array('POST' => 0), null),
                    '/api/user/editor/' => array(array('_route' => 'userEdit', '_controller' => 'App\\Controller\\ApiController::setUserData'), null, array('POST' => 0), null),
                    '/api/server/optimize_photo' => array(array('_route' => 'optimizePhoto', '_controller' => 'App\\Controller\\ApiController::optimizePhoto'), null, array('GET' => 0), null),
                    '/api/auth/' => array(array('_route' => 'auth', '_controller' => 'App\\Controller\\AuthController::authAction'), null, array('POST' => 0), null),
                    '/api/logout/' => array(array('_route' => 'logout', '_controller' => 'App\\Controller\\AuthController::logoutAction'), null, array('GET' => 0), null),
                    '/' => array(array('_route' => 'page:index', '_controller' => 'App\\Controller\\MainController::index'), null, null, null),
                    '/list/' => array(array('_route' => 'page:list', '_controller' => 'App\\Controller\\MainController::list'), null, null, null),
                    '/create/' => array(array('_route' => 'page:create:item', '_controller' => 'App\\Controller\\MainController::create'), null, null, null),
                    '/logout/' => array(array('_route' => 'page:logout', '_controller' => 'App\\Controller\\MainController::logout'), null, null, null),
                    '/cabinet/' => array(array('_route' => 'page:cabinet', '_controller' => 'App\\Controller\\MainController::cabinet'), null, null, null),
                    '/settings/' => array(array('_route' => 'page:settings', '_controller' => 'App\\Controller\\MainController::settings'), null, null, null),
                    '/_profiler/' => array(array('_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'), null, null, null),
                    '/_profiler/search' => array(array('_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'), null, null, null),
                    '/_profiler/search_bar' => array(array('_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'), null, null, null),
                    '/_profiler/phpinfo' => array(array('_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'), null, null, null),
                    '/_profiler/open' => array(array('_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'), null, null, null),
                );

                if (!isset($routes[$pathinfo])) {
                    break;
                }
                list($ret, $requiredHost, $requiredMethods, $requiredSchemes) = $routes[$pathinfo];

                $hasRequiredScheme = !$requiredSchemes || isset($requiredSchemes[$context->getScheme()]);
                if ($requiredMethods && !isset($requiredMethods[$canonicalMethod]) && !isset($requiredMethods[$requestMethod])) {
                    if ($hasRequiredScheme) {
                        $allow += $requiredMethods;
                    }
                    break;
                }
                if (!$hasRequiredScheme) {
                    $allowSchemes += $requiredSchemes;
                    break;
                }

                return $ret;
        }

        $matchedPathinfo = $pathinfo;
        $regexList = array(
            0 => '{^(?'
                    .'|/api/(?'
                        .'|user/([^/]++)/(?'
                            .'|visibles/(*:41)'
                            .'|stat/(*:53)'
                        .')'
                        .'|item(?'
                            .'|/(?'
                                .'|edit/([^/]++)/(*:86)'
                                .'|delete/([^/]++)(*:108)'
                            .')'
                            .'|s/([^/]++)/(*:128)'
                        .')'
                        .'|photo/([^/]++)/(*:152)'
                    .')'
                    .'|/list/([^/]++)/(*:176)'
                    .'|/edit/([^/]++)/(*:199)'
                    .'|/upload/([^/]++)/(*:224)'
                    .'|/_(?'
                        .'|error/(\\d+)(?:\\.([^/]++))?(*:263)'
                        .'|wdt/([^/]++)(*:283)'
                        .'|profiler/([^/]++)(?'
                            .'|/(?'
                                .'|search/results(*:329)'
                                .'|router(*:343)'
                                .'|exception(?'
                                    .'|(*:363)'
                                    .'|\\.css(*:376)'
                                .')'
                            .')'
                            .'|(*:386)'
                        .')'
                    .')'
                .')$}sD',
        );

        foreach ($regexList as $offset => $regex) {
            while (preg_match($regex, $matchedPathinfo, $matches)) {
                switch ($m = (int) $matches['MARK']) {
                    default:
                        $routes = array(
                            41 => array(array('_route' => 'setUserVisibles', '_controller' => 'App\\Controller\\ApiController::setUserVisibles'), array('id'), array('POST' => 0), null),
                            53 => array(array('_route' => 'userStat', '_controller' => 'App\\Controller\\ApiController::getStatData'), array('login'), array('GET' => 0), null),
                            86 => array(array('_route' => 'itemEdit', '_controller' => 'App\\Controller\\ApiController::editorInit'), array('itemId'), array('GET' => 0), null),
                            108 => array(array('_route' => 'itemDelete', '_controller' => 'App\\Controller\\ApiController::deleteItem'), array('itemId'), array('DELETE' => 0), null),
                            128 => array(array('_route' => 'item', '_controller' => 'App\\Controller\\ApiController::getItem'), array('itemId'), array('GET' => 0), null),
                            152 => array(array('_route' => 'photoDelete', '_controller' => 'App\\Controller\\ApiController::deletePhoto'), array('photoId'), array('DELETE' => 0), null),
                            176 => array(array('_route' => 'page:list:item', '_controller' => 'App\\Controller\\MainController::item'), array('itemId'), null, null),
                            199 => array(array('_route' => 'page:edit:item', '_controller' => 'App\\Controller\\MainController::edit'), array('itemId'), null, null),
                            224 => array(array('_route' => 'upload', '_controller' => 'App\\Controller\\UploadController::upload'), array('folderId'), array('POST' => 0), null),
                            263 => array(array('_route' => '_twig_error_test', '_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'), array('code', '_format'), null, null),
                            283 => array(array('_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'), array('token'), null, null),
                            329 => array(array('_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'), array('token'), null, null),
                            343 => array(array('_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'), array('token'), null, null),
                            363 => array(array('_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception::showAction'), array('token'), null, null),
                            376 => array(array('_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception::cssAction'), array('token'), null, null),
                            386 => array(array('_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'), array('token'), null, null),
                        );

                        list($ret, $vars, $requiredMethods, $requiredSchemes) = $routes[$m];

                        foreach ($vars as $i => $v) {
                            if (isset($matches[1 + $i])) {
                                $ret[$v] = $matches[1 + $i];
                            }
                        }

                        $hasRequiredScheme = !$requiredSchemes || isset($requiredSchemes[$context->getScheme()]);
                        if ($requiredMethods && !isset($requiredMethods[$canonicalMethod]) && !isset($requiredMethods[$requestMethod])) {
                            if ($hasRequiredScheme) {
                                $allow += $requiredMethods;
                            }
                            break;
                        }
                        if (!$hasRequiredScheme) {
                            $allowSchemes += $requiredSchemes;
                            break;
                        }

                        return $ret;
                }

                if (386 === $m) {
                    break;
                }
                $regex = substr_replace($regex, 'F', $m - $offset, 1 + strlen($m));
                $offset += strlen($m);
            }
        }
        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        return null;
    }
}
