<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcDevDebugProjectContainerUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes;
    private $defaultLocale;

    public function __construct(RequestContext $context, LoggerInterface $logger = null, string $defaultLocale = null)
    {
        $this->context = $context;
        $this->logger = $logger;
        $this->defaultLocale = $defaultLocale;
        if (null === self::$declaredRoutes) {
            self::$declaredRoutes = array(
        'init' => array(array(), array('_controller' => 'App\\Controller\\ApiController::initApp'), array(), array(array('text', '/api/init/')), array(), array()),
        'itemCreate' => array(array(), array('_controller' => 'App\\Controller\\ApiController::createItem'), array(), array(array('text', '/api/item/')), array(), array()),
        'galleryCreate' => array(array(), array('_controller' => 'App\\Controller\\ApiController::createGallery'), array(), array(array('text', '/api/gallery/')), array(), array()),
        'setUserVisibles' => array(array('id'), array('_controller' => 'App\\Controller\\ApiController::setUserVisibles'), array(), array(array('text', '/visibles/'), array('variable', '/', '[^/]++', 'id'), array('text', '/api/user')), array(), array()),
        'allCountries' => array(array(), array('_controller' => 'App\\Controller\\ApiController::getAllCountries'), array(), array(array('text', '/api/countries/')), array(), array()),
        'itemEdit' => array(array('itemId'), array('_controller' => 'App\\Controller\\ApiController::editorInit'), array(), array(array('text', '/'), array('variable', '/', '[^/]++', 'itemId'), array('text', '/api/item/edit')), array(), array()),
        'itemDelete' => array(array('itemId'), array('_controller' => 'App\\Controller\\ApiController::deleteItem'), array(), array(array('variable', '/', '[^/]++', 'itemId'), array('text', '/api/item/delete')), array(), array()),
        'photoDelete' => array(array('photoId'), array('_controller' => 'App\\Controller\\ApiController::deletePhoto'), array(), array(array('text', '/'), array('variable', '/', '[^/]++', 'photoId'), array('text', '/api/photo')), array(), array()),
        'items' => array(array(), array('_controller' => 'App\\Controller\\ApiController::getItems'), array(), array(array('text', '/api/items/')), array(), array()),
        'item' => array(array('itemId'), array('_controller' => 'App\\Controller\\ApiController::getItem'), array(), array(array('text', '/'), array('variable', '/', '[^/]++', 'itemId'), array('text', '/api/items')), array(), array()),
        'createOptions' => array(array(), array('_controller' => 'App\\Controller\\ApiController::createOptions'), array(), array(array('text', '/api/options/')), array(), array()),
        'userEdit' => array(array(), array('_controller' => 'App\\Controller\\ApiController::setUserData'), array(), array(array('text', '/api/user/editor/')), array(), array()),
        'userStat' => array(array('login'), array('_controller' => 'App\\Controller\\ApiController::getStatData'), array(), array(array('text', '/stat/'), array('variable', '/', '[^/]++', 'login'), array('text', '/api/user')), array(), array()),
        'optimizePhoto' => array(array(), array('_controller' => 'App\\Controller\\ApiController::optimizePhoto'), array(), array(array('text', '/api/server/optimize_photo')), array(), array()),
        'auth' => array(array(), array('_controller' => 'App\\Controller\\AuthController::authAction'), array(), array(array('text', '/api/auth/')), array(), array()),
        'logout' => array(array(), array('_controller' => 'App\\Controller\\AuthController::logoutAction'), array(), array(array('text', '/api/logout/')), array(), array()),
        'page:index' => array(array(), array('_controller' => 'App\\Controller\\MainController::index'), array(), array(array('text', '/')), array(), array()),
        'page:list' => array(array(), array('_controller' => 'App\\Controller\\MainController::list'), array(), array(array('text', '/list/')), array(), array()),
        'page:list:item' => array(array('itemId'), array('_controller' => 'App\\Controller\\MainController::item'), array(), array(array('text', '/'), array('variable', '/', '[^/]++', 'itemId'), array('text', '/list')), array(), array()),
        'page:edit:item' => array(array('itemId'), array('_controller' => 'App\\Controller\\MainController::edit'), array(), array(array('text', '/'), array('variable', '/', '[^/]++', 'itemId'), array('text', '/edit')), array(), array()),
        'page:create:item' => array(array(), array('_controller' => 'App\\Controller\\MainController::create'), array(), array(array('text', '/create/')), array(), array()),
        'page:logout' => array(array(), array('_controller' => 'App\\Controller\\MainController::logout'), array(), array(array('text', '/logout/')), array(), array()),
        'page:cabinet' => array(array(), array('_controller' => 'App\\Controller\\MainController::cabinet'), array(), array(array('text', '/cabinet/')), array(), array()),
        'page:settings' => array(array(), array('_controller' => 'App\\Controller\\MainController::settings'), array(), array(array('text', '/settings/')), array(), array()),
        'upload' => array(array('folderId'), array('_controller' => 'App\\Controller\\UploadController::upload'), array(), array(array('text', '/'), array('variable', '/', '[^/]++', 'folderId'), array('text', '/upload')), array(), array()),
        '_twig_error_test' => array(array('code', '_format'), array('_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'), array('code' => '\\d+'), array(array('variable', '.', '[^/]++', '_format'), array('variable', '/', '\\d+', 'code'), array('text', '/_error')), array(), array()),
        '_wdt' => array(array('token'), array('_controller' => 'web_profiler.controller.profiler::toolbarAction'), array(), array(array('variable', '/', '[^/]++', 'token'), array('text', '/_wdt')), array(), array()),
        '_profiler_home' => array(array(), array('_controller' => 'web_profiler.controller.profiler::homeAction'), array(), array(array('text', '/_profiler/')), array(), array()),
        '_profiler_search' => array(array(), array('_controller' => 'web_profiler.controller.profiler::searchAction'), array(), array(array('text', '/_profiler/search')), array(), array()),
        '_profiler_search_bar' => array(array(), array('_controller' => 'web_profiler.controller.profiler::searchBarAction'), array(), array(array('text', '/_profiler/search_bar')), array(), array()),
        '_profiler_phpinfo' => array(array(), array('_controller' => 'web_profiler.controller.profiler::phpinfoAction'), array(), array(array('text', '/_profiler/phpinfo')), array(), array()),
        '_profiler_search_results' => array(array('token'), array('_controller' => 'web_profiler.controller.profiler::searchResultsAction'), array(), array(array('text', '/search/results'), array('variable', '/', '[^/]++', 'token'), array('text', '/_profiler')), array(), array()),
        '_profiler_open_file' => array(array(), array('_controller' => 'web_profiler.controller.profiler::openAction'), array(), array(array('text', '/_profiler/open')), array(), array()),
        '_profiler' => array(array('token'), array('_controller' => 'web_profiler.controller.profiler::panelAction'), array(), array(array('variable', '/', '[^/]++', 'token'), array('text', '/_profiler')), array(), array()),
        '_profiler_router' => array(array('token'), array('_controller' => 'web_profiler.controller.router::panelAction'), array(), array(array('text', '/router'), array('variable', '/', '[^/]++', 'token'), array('text', '/_profiler')), array(), array()),
        '_profiler_exception' => array(array('token'), array('_controller' => 'web_profiler.controller.exception::showAction'), array(), array(array('text', '/exception'), array('variable', '/', '[^/]++', 'token'), array('text', '/_profiler')), array(), array()),
        '_profiler_exception_css' => array(array('token'), array('_controller' => 'web_profiler.controller.exception::cssAction'), array(), array(array('text', '/exception.css'), array('variable', '/', '[^/]++', 'token'), array('text', '/_profiler')), array(), array()),
    );
        }
    }

    public function generate($name, $parameters = array(), $referenceType = self::ABSOLUTE_PATH)
    {
        $locale = $parameters['_locale']
            ?? $this->context->getParameter('_locale')
            ?: $this->defaultLocale;

        if (null !== $locale && (self::$declaredRoutes[$name.'.'.$locale][1]['_canonical_route'] ?? null) === $name) {
            unset($parameters['_locale']);
            $name .= '.'.$locale;
        } elseif (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
