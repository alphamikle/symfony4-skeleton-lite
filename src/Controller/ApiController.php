<?php

namespace App\Controller;

use App\Service\Security\User;
use Propel\Runtime\ActiveQuery\Criteria;
use src\Model\Category;
use src\Model\CategoryQuery;
use src\Model\Country;
use src\Model\CountryQuery;
use src\Model\DefaultCountryQuery;
use src\Model\FileQuery;
use src\Model\FolderFileQuery;
use src\Model\FolderQuery;
use src\Model\Item;
use src\Model\ItemQuery;
use src\Model\Material;
use src\Model\MaterialQuery;
use src\Model\Subcategory;
use src\Model\SubcategoryQuery;
use src\Model\UserQuery;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Email;

class ApiController extends Controller
{
    /**
     * @Route("/api/init/", name="init", methods={"GET"})
     *
     * @return JsonResponse
     * @throws \Propel\Runtime\Exception\PropelException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function initApp()
    {
        $userProvider = $this->container->get('user_provider');
        $fileService = $this->container->get('service.file');
        $itemService = $this->container->get('service.item');
        $response = [];
        $response['isAuth'] = $userProvider->isAuth();
        $response['options'] = $itemService->getAdderOptions();
        if ($response['isAuth']) {
            $currentUser = $userProvider->getCurrentUser();
            $response['user']['userName'] = $currentUser->getUsername();
            $response['user']['email'] = $currentUser->getEmail();
            $response['user']['birthday'] = $currentUser->getBirthday('d-m-Y');
            $response['user']['registeredAt'] = $currentUser->getRegistrationDate('d-m-Y');
            $response['user']['photo'] = $currentUser->getGalleryId() ? isset($fileService->getFolderData($currentUser->getGalleryId())[0]) ? $fileService->getFolderData($currentUser->getGalleryId())[0] : [] : [];
            $response['user']['background'] = $currentUser->getBackgroundGalleryId() ? isset($fileService->getFolderData($currentUser->getBackgroundGalleryId())[0]) ? $fileService->getFolderData($currentUser->getBackgroundGalleryId())[0] : [] : [];
            $response['user']['id'] = $currentUser->getId();
            $response['user']['galleryId'] = $currentUser->getGalleryId();
            $galleryId = $currentUser->getBackgroundGalleryId();
            if (!$galleryId) {
                $galleryId = $fileService->createFolder()->getId();
                $currentUser->setBackgroundGalleryId($galleryId)->save();
            }
            $response['user']['backgroundGalleryId'] = $galleryId;

            $visibles = $currentUser->getVisibles();
            if ($visibles === '') {
                $visibles = array_map(function ($category) {
                    $category['visible'] = true;
                    return $category;
                }, $response['options']['categories']);
                $currentUser->setVisibles(json_encode($visibles))->save();
            } else {
                $visibles = (array)json_decode($visibles);
                $isAnyNew = false;
                for ($k = 0; $k < count($response['options']['categories']); $k++) {
                    $option = $response['options']['categories'][$k];
                    $isFinded = false;
                    for ($i = 0; $i < count($visibles); $i++) {
                        $visibility = (array)$visibles[$i];
                        if ($option['id'] === $visibility['id']) {
                            $visibility['title'] = $option['title'];
                            $isFinded = true;
                        }
                    }
                    if (!$isFinded) {
                        $isAnyNew = true;
                        $option['visible'] = true;
                        $visibles[] = $option;
                    }
                }
                if ($isAnyNew) {
                    $currentUser->setVisibles(json_encode($visibles))->save();
                }
            }
            $response['user']['visibles'] = $visibles;
        }
        $routesCollection = $this->container->get('router')->getRouteCollection();
        foreach ($routesCollection as $key => $route) {
            /**@var $route Route*/
            $path = $route->getPath();
            if (preg_match('/api/', $path)) {
                $response['api'][$key]['path'] = $path;
                $response['api'][$key]['method'] = $route->getMethods()[0];
                if (preg_match('/\\{\\w*\\}/', $path, $matches)) {
                    $response['api'][$key]['param'] = $matches[0];
                }
            }
        }
        return $this->json($response);
    }

    /**
     * Создать новый элемент
     * @Route ("/api/item/", name="itemCreate", methods={"POST"})
     * @param $request Request
     * @return JsonResponse
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function createItem(Request $request)
    {
        $requestContent = json_decode($request->getContent());
        $cache = $this->get('service.cache');
        if (isset($requestContent->id)) {
            $cache->delete($cache::CACHE_ITEM_DATA . $requestContent->id);
            $cache->delete($cache::CACHE_ITEM_FULL_DATA . $requestContent->id);
        }

        $country = $requestContent->country;
        $material = $requestContent->material;
        $title = $requestContent->title;
        $images = $requestContent->images;
        $price = $requestContent->price ?: 0;
        $isExchange = $requestContent->isExchange ?: 0;
        $description = $requestContent->description ?: '';
        $category = $requestContent->category;
        $subcategory = $requestContent->subcategory;
        $galleryId = $requestContent->galleryId;

        $rightIds = [];
        $userProvider = $this->get('user_provider');
        $fileService = $this->get('service.file');
        foreach ($images as $image) {
            $dFile = $image;
            if (!$galleryId) {
                $galleryId = $dFile->folder_id;
            }
            $rightIds[] = $dFile->id;
        }
        if (isset($requestContent->id) || $requestContent->id) {
            $newItem = ItemQuery::create()->findPk($requestContent->id);
        } else {
            $newItem = new Item();
        }
        $newItem
            ->setCountryId($country ?: null)
            ->setMaterialId($material ?: null)
            ->setCategoryId($category ?: null)
            ->setSubcategoryId($subcategory ?: null)
            ->setTitle($title)
            ->setPrice($price)
            ->setDescription($description)
            ->setIsExchange($isExchange)
            ->setGalleryId($galleryId)
            ->setUser($userProvider->getCurrentUser())
            ->save();

        $this->get('session')->set('folder', null);
        $folder = FolderQuery::create()->findPk($galleryId);
        $fileService->deleteExcessFiles($folder, $rightIds);
        $response = [
            'message' => 'Элемент добавлен в коллекцию'
        ];
        return $this->json($response);
    }

    /**
     * Создать новую галлерею
     * @Route ("/api/gallery/", name="galleryCreate", methods={"POST"})
     * @param $request Request
     * @return JsonResponse
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function createGallery(Request $request)
    {
        $fileService = $this->get('service.file');
        $userId = isset(json_decode($request->getContent(), true)['userId']) ? json_decode($request->getContent(), true)['userId'] : null;
        $folder = $fileService->createFolder($userId);
        return $this->json($folder->getId());
    }

    /**
     * Изменяет настройки видимости предметов в коллекциях
     * @Route ("/api/user/{id}/visibles/", name="setUserVisibles", methods={"POST"})
     * @param $request Request
     * @return JsonResponse
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setUserVisibles(Request $request)
    {
        $cache = $this->get('service.cache');
        $cache->delete($cache::CACHE_ADDER_OPTIONS);
        $fields = json_decode($request->getContent(), true);
        $userProvider = $this->container->get('user_provider');
        $response['isAuth'] = $userProvider->isAuth();
        if ($response['isAuth']) {
            $currentUser = $userProvider->getCurrentUser();
            $currentUser->setVisibles(json_encode($fields['visibles']))->save();
        }
        return $this->json(1);
    }

    /**
     * @Route("/api/countries/", name="allCountries", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getAllCountries(Request $request)
    {
        return $this->json(DefaultCountryQuery::create()->getDefaultCountryOptions());
    }

    /**
     * Возвращает данные, пригодные для вставки в редактор элементов
     * @Route("/api/item/edit/{itemId}/", name="itemEdit", methods={"GET"})
     * @param Request $request
     * @param int $itemId
     * @return JsonResponse
     * @throws \Propel\Runtime\Exception\PropelException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function editorInit(Request $request, $itemId)
    {
        $itemData = $this->get('service.item')->getFullItemData($itemId);
        return $this->json($itemData);
    }

    /**
     * Удаляет элемент по id
     * @Route ("/api/item/delete/{itemId}", name="itemDelete", methods={"DELETE"})
     *
     * @param $request Request
     * @throws
     * @return JsonResponse
     */
    public function deleteItem(Request $request, $itemId)
    {
        $userProvider = $this->get('user_provider');
        $user = $userProvider->getCurrentUser();
        $item = ItemQuery::create()->filterById($itemId)->filterByUser($user)->findOne();
        if ($item) {
            $item->delete();
            $response = [
                'message' => 'Вы удалили элемент',
            ];
            $cache = $this->get('service.cache');
            $cache->delete($cache::CACHE_ITEM_DATA . $itemId);
            return $this->json($response);
        } else {
            $response = [
                'error' => 'Нельзя удалить предмет'
            ];
            return $this->json($response, 400);
        }
    }

    /**
     * @Route ("/api/photo/{photoId}/", name="photoDelete", methods={"DELETE"})
     * @param Request $request
     * @param $photoId int
     * @return JsonResponse
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function deletePhoto(Request $request, $photoId)
    {
        FolderFileQuery::create()->filterByFileId($photoId)->delete();
        return $this->json('OK');
    }

    /**
     * Возвращает список товаров с фильтрацией по query-параметрам
     * @Route("/api/items/", name="items", methods={"GET"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getItems(Request $request)
    {
        $itemService = $this->get('service.item');
        $items = $itemService->getItemsAccordingRequest($request);
        return $this->json($items);
    }

    /**
     * @Route("/api/items/{itemId}/", name="item", methods={"GET"})
     * @param Request $request
     * @param $itemId int
     * @return JsonResponse
     * @throws \Propel\Runtime\Exception\PropelException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getItem(Request $request, $itemId)
    {
        $itemService = $this->get('service.item');
        return $this->json($itemService->getFullItemData($itemId));
    }

    /**
     * Добавляет параметры item'ов
     * материал, страна или тип (возможно еще)
     * @Route("/api/options/", name="createOptions", methods={"POST"})
     * @param $request Request
     * @return JsonResponse
     * @throws
     */
    public function createOptions(Request $request)
    {
        $cache = $this->get('service.cache');
        $cache->delete($cache::CACHE_ADDER_OPTIONS);
        $itemService = $this->container->get('service.item');

        $fields = json_decode($request->getContent(), true);
        $errors = $itemService->validateNewOptions($fields);

        if (!count($errors)) {
            $itemService->createNewOptions($fields);
            $options = $itemService->getAdderOptions();
            return $this->json($options);
        } else {
            return $this->json($errors, 400);
        }
    }

    /**
     * Редактирование данных пользователя
     * @Route("/api/user/editor/", name="userEdit", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setUserData(Request $request)
    {
        $jRequest = json_decode($request->getContent());
        $userService = $this->get('user_provider');
        $user = $userService->getCurrentUser();
        $email = isset($jRequest->email) ? $jRequest->email : null;
        $birthday = isset($jRequest->birthday) ? $jRequest->birthday : null;
        $username = isset($jRequest->nick) ? $jRequest->nick : null;
        $response = [];
        if ($email) {
            if ($this->get('validator')->validate($email, new Email())->has(0)) {
                $response['errors']['email'] = 'Неверный формат email';
            } elseif ($user->getEmail() == $email) {
                $response['errors']['email'] = 'Вы вводите свой email';
            } elseif (UserQuery::create()->filterByEmail($email)->count()) {
                $response['errors']['email'] = 'Данный email принадлежит другому пользователю';
            } else {
                $user->setEmail($jRequest->email)->save();
            }
        }

        if ($birthday) {
            if ($this->get('validator')->validate($birthday, new Date())->has(0)) {
                $response['errors']['birthday'] = 'Неверный формат даты';
            } else {
                $user->setBirthday($birthday)->save();
            }
        }

        if ($username) {
            if (UserQuery::create()->filterByUsername($username)->count()) {
                $response['errors']['nick'] = 'Данный ник уже занят';
            } else {
                $user->setUsername($username)->save();
            }
        }

        if (!isset($response['errors'])) {
            $response['success']['message'] = 'Ок';
            $this->container->get('service.cache')->clear();
            return $this->json($response);
        } else {
            return $this->json($response, 400);
        }
    }

    /**
     * Возвращает статистику по товарам пользователя
     * @Route("/api/user/{login}/stat/", name="userStat", methods={"GET"})
     * @param Request $request
     * @param $login string
     * @return JsonResponse
     */
    public function getStatData(Request $request, $login)
    {
        $userId = UserQuery::create()->findOneByUsername($login)->getId();
        $userItems = ItemQuery::create()->filterByUserId($userId)->orderByCreatedAt(Criteria::DESC)->find();
        $countries = CountryQuery::create()->orderByTitle(Criteria::ASC)->find();
        $materials = MaterialQuery::create()->orderByTitle(Criteria::ASC)->find();
        $categories = CategoryQuery::create()->orderByTitle(Criteria::ASC)->find();
        $subcategories = SubcategoryQuery::create()->orderByTitle(Criteria::ASC)->find();

        $out = [];

        /*Страны*/
        foreach ($countries as $country) {
            $countItems = 0;
            foreach ($userItems as $item) {
                if ($item->getCountryId() == $country->getId()) {
                    $countItems++;
                }
            }
            if ($countItems) {
                $out['Страны'][] = [
                    'title' => $country->getTitle(),
                    'image' => $country->getFlag(),
                    'items_count' => $countItems
                ];
            }
        }

        /*Материалы*/
        foreach ($materials as $material) {
            $countItems = 0;
            foreach ($userItems as $item) {
                if ($item->getMaterialId() == $material->getId()) {
                    $countItems++;
                }
            }
            if ($countItems) {
                $out['Материалы'][] = [
                    'title' => $material->getTitle(),
                    'items_count' => $countItems
                ];
            }
        }

        /*Категории*/
        foreach ($categories as $category) {
            $countItems = 0;
            foreach ($userItems as $item) {
                if ($item->getCategoryId() == $category->getId()) {
                    $countItems++;
                }
            }
            if ($countItems) {
                $out['Категории'][] = [
                    'title' => $category->getTitle(),
                    'items_count' => $countItems
                ];
            }
        }

        /*Подкатегории*/
        foreach ($subcategories as $subcategory) {
            $countItems = 0;
            foreach ($userItems as $item) {
                if ($item->getCategoryId() == $subcategory->getId()) {
                    $countItems++;
                }
            }
            if ($countItems) {
                $out['Подкатегории'][] = [
                    'title' => $subcategory->getTitle(),
                    'items_count' => $countItems
                ];
            }
        }

        /*Обмен*/
        $out['Обмен'] = 0;
        foreach ($userItems as $item) {
            if ($item->getIsExchange() == true) {
                $out['Обмен']++;
            }
        }

        /*Всего*/
        $out['Всего'] = 0;
        foreach ($userItems as $item) {
            $out['Всего']++;
        }

        return $this->json($out);
    }

    /**
     * Удаляет лишние фотографии и оптимизирует старые
     * @Route("/api/server/optimize_photo", name="optimizePhoto", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function optimizePhoto(Request $request)
    {
        set_time_limit(0);
        $uploadsDir = __DIR__ . '/../../public/uploads';
        $activePhotos = FileQuery::create()->select(['Url'])->find()->toArray();
        $activePhotos = array_map(function ($url) {
            return str_replace('/uploads/', '', $url);
        }, $activePhotos);
        $allPhotos = scandir($uploadsDir);
        $allPhotos = array_filter($allPhotos, function ($url) {
           return $url !== '.' && $url !== '..';
        });
        $allPhotos = array_values($allPhotos);
        $diff = array_values(array_diff($allPhotos, $activePhotos));
        for($i = 0; $i < count($diff); $i++) {
            unlink($uploadsDir . '/' . $diff[$i]);
        }
        $gregwar = $this->container->get('imager');
        for ($i = 0; $i < count($activePhotos); $i++) {
            $imageName = $activePhotos[$i];
            $fileName = $uploadsDir . '/' . $imageName;
            if (is_file($fileName)) {
//                $file = fopen($fileName, 'w');
                $gregwar->open($fileName)->cropResize(900, 900)->save($fileName);
//                fclose($file);
            }
        }
        return $this->json($activePhotos);
    }
}
