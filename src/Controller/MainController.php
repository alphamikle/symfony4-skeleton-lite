<?php

namespace App\Controller;

use src\Model\ItemQuery;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends Controller
{
    /**
     * Форма авторизации
     * @Route ("/", name="page:index")
     * @throws
     */
    public function index()
    {
        $userProvider = $this->get('user_provider');
        if ($userProvider->isAuth()) {
            return $this->redirectToRoute('page:list');
        }
        return $this->render('index.html.twig', []);
    }

    /**
     * @Route ("/list/", name="page:list")
     * @throws
     */
    public function list()
    {
        return $this->render('index.html.twig', [

        ]);
    }

    /**
     * @Route ("/list/{itemId}/", name="page:list:item")
     * @param $itemId integer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function item($itemId)
    {
        if (!ItemQuery::create()->filterById($itemId)->count()) {
            return $this->redirectToRoute('page:index');
        }
        return $this->render('index.html.twig', [

        ]);
    }

    /**
     * @Route ("/edit/{itemId}/", name="page:edit:item")
     * @param $itemId integer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit($itemId)
    {
        if (!ItemQuery::create()->filterById($itemId)->count()) {
            return $this->redirectToRoute('page:index');
        }
        return $this->render('index.html.twig', [

        ]);
    }

    /**
     * @Route ("/create/", name="page:create:item")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function create()
    {
        return $this->render('index.html.twig', [

        ]);
    }

    /**
     * @Route ("/logout/", name="page:logout")
     * @throws
     */
    public function logout()
    {
        return $this->redirectToRoute('logout');
    }

    /**
     * Кабинет
     * @Route ("/cabinet/", name="page:cabinet")
     * @throws
     */
    public function cabinet()
    {
        return $this->render('index.html.twig', [

        ]);
    }

    /**
     * Настройки
     * @Route ("/settings/", name="page:settings")
     * @throws
     */
    public function settings()
    {
        return $this->render('index.html.twig', [

        ]);
    }
}