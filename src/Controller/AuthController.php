<?php

namespace App\Controller;

use src\Model\User as UserModel;
use App\Service\Security\User;
use src\Model\UserQuery;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AuthController extends Controller
{
    /**
     * Аутентификация пользователя по email
     * @Route("/api/auth/", name="auth", methods={"POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function authAction(Request $request)
    {
        $isReg = $request->get('isReg');
        $email = $request->get('login');
        $password = $request->get('password');
        $passwordReturn = $request->get('rePassword');
        $encoder = $this->get('security.password_encoder');

        //Происходит регистрация пользователя
        if ($isReg && $email && $password && $passwordReturn) {
            if ($password !== $passwordReturn) {
                $response['errors']['password'] = [
                    'title' => 'Пароли должны совпадать'
                ];
                $response['errors']['rePassword'] = $response['errors']['password'];
            }
            if (UserQuery::create()->findOneByEmail($email)) {
                $response['errors']['login'] = [
                    'title' => 'Такой пользователь уже зарегистрирован'
                ];
            }
            if (isset($response['errors'])) {
                return $this->json($response, 400);
            }
            $user = new User();
            $userModel = new UserModel();
            $hash = $this->get('security.password_encoder')->encodePassword($user, $password);

            $userModel
                ->setEmail($email)
                ->setUsername(preg_replace('/@.*$/ui', '', $email))
                ->setPassword($hash)
                ->setVisibles('{}')
                ->setRegistrationDate(time())
                ->save();

            $response = [
                'title' => 'Вы успешно зарегистрировались!',
                'message' => 'Теперь вы можете войти в свой профиль'
            ];

            return $this->json($response);

        } elseif ($email && $password) {
            if ($user = UserQuery::create()->findOneByEmail($email)) {
                $userCore = new User();
                $userPassword = $user->getPassword();
                $hash = $encoder->encodePassword($userCore, $password);

                if ($userPassword == $hash) {
                    $this->get('service.authorization')->authorizeUser($user);
                    $response['initData'] = json_decode($this->forward('\App\Controller\ApiController::initApp')->getContent(), true);
                    $response['notification'] = [
                        'title' => 'Вы успешно вошли на сайт',
                    ];
                    return $this->json($response, 200);
                } else {
                    $response['errors']['login'] = [
                        'title' => 'Неправильный логин или пароль.'
                    ];
                    $response['errors']['password'] = $response['errors']['login'];
                    return $this->json($response, 400);
                }
            } else {
                $response['errors']['login'] = [
                    'title' => 'Не найден пользователь с таким Email'
                ];
                $response['errors']['password'] = $response['errors']['login'];
                return $this->json($response, 400);
            }
        }
        throw new NotFoundHttpException();
    }

    /**
     * @Route("/api/logout/", name="logout", methods={"GET"})
     */
    public function logoutAction()
    {
        //Просто разлогинит пользователя и переведет на страницу index из параметров в security
        exit;
    }
}
