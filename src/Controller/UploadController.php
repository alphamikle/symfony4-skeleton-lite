<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UploadController extends Controller
{
    /**
     * @Route ("/upload/{folderId}/", name="upload", methods={"POST"})
     * @param $request Request
     * @param $folderId int
     * @return string
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function upload(Request $request, int $folderId)
    {
        $fileService = $this->get('service.file');
        $savedFiles = $fileService->uploadFiles($request, $folderId);
        return $this->json($savedFiles);
    }
}