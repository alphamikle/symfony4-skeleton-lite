<?php

namespace src\Model;

use src\Model\Base\Country as BaseCountry;

/**
 * Skeleton subclass for representing a row from the 'country' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Country extends BaseCountry
{
    public function getFlag()
    {
        $code = $this->getCode();
        $filename = $code . '.png';
        $flagsDir = __DIR__ . '/../../public/images/flags/';
        if (is_file($flagsDir . $filename)) {
            return '/images/flags/' . $filename;
        } else {
            return '';
        }
    }

    public function copyDefaultCountry($defaultCountryId)
    {
        $defaultCountry = CountryQuery::create()->getDefaultCountry($defaultCountryId);
        $this
            ->setCode($defaultCountry->getCode())
            ->setTitle($defaultCountry->getTitle());
        return $this;
    }
}
