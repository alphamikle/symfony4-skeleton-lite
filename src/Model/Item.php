<?php

namespace src\Model;

use src\Model\Base\Item as BaseItem;

/**
 * Skeleton subclass for representing a row from the 'item' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Item extends BaseItem
{
    /**
     * @param null $limit
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getFiles($limit = null)
    {
        $folder = $this->getFolder();
        if ($folder) {
            $files = FileQuery::create()
                ->useFolderFileQuery()
                ->filterByFolder($folder)
                ->endUse()
                ->select('Url')
                ->_if($limit !== null)
                ->limit($limit)
                ->_endif()
                ->find()
                ->toArray()
            ;
            return $files;
        } else {
            return [];
        }
    }

    /**
     * Возвращает первый файл - изображение обложку
     * @return string
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getCover()
    {
        $files = $this->getFiles(1);
        if (empty($files)) {
            return '/images/fallbackItemCover.png';
        } else {
            return $files[0];
        }
    }
}
