<?php

namespace src\Model;

use Propel\Runtime\ActiveQuery\Criteria;
use src\Model\Base\CategoryQuery as BaseCategoryQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'category' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CategoryQuery extends BaseCategoryQuery
{
    /**
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getCategoryOptions($notVisibles = [])
    {
        return $this
            ->filterById($notVisibles, Criteria::NOT_IN)
            ->orderByTitle(Criteria::ASC)
            ->withColumn('Id', 'id')
            ->withColumn('Title', 'title')
            ->select(['id', 'title'])
            ->find()
            ->toArray();
    }

}
