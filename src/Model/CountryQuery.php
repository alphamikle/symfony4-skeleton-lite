<?php

namespace src\Model;

use Propel\Runtime\ActiveQuery\Criteria;
use src\Model\Base\CountryQuery as BaseCountryQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'country' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CountryQuery extends BaseCountryQuery
{
    private $defaultCountry = null;

    /**
     * @return array
     * @throws
     */
    public function getCountryOptions()
    {
        $rawArray = $this
            ->orderByTitle(Criteria::ASC)
            ->withColumn('Id', 'id')
            ->withColumn('Title', 'title')
            ->withColumn('Code', 'code')
            ->select(['id', 'title', 'code'])
            ->find()
            ->toArray();

        $preparedArray = array_map(function ($item) {
            if (mb_strlen($item['title']) > 14) {
                $item['title'] = mb_substr($item['title'], 0, 13) . '.';
            }
            $item['title'] = mb_strtolower($item['title']);
            $item['title'] = mb_strtoupper(mb_substr($item['title'], 0, 1)) . mb_substr($item['title'], 1, 14);
            $item['count'] = CountryQuery::create()->findPk($item['id'])->countItems();
            $item['image'] = CountryQuery::create()->findPk($item['id'])->getFlag();
            return $item;
        }, $rawArray);
        usort($preparedArray, function ($a, $b) {
            if ($a['count'] == $b['count']) {
                return 0;
            }
            return ($b['count'] < $a['count']) ? -1 : 1;
        });
        return $preparedArray;
    }

    public function issetCountry($defauldCountryId)
    {
        $this->defaultCountry = DefaultCountryQuery::create()->findPk($defauldCountryId);
        return (bool) $this->filterByCode($this->defaultCountry->getCode())->count();
    }

    public function getDefaultCountry($defaultCountryId)
    {
        return $this->defaultCountry === null ? DefaultCountryQuery::create()->findPk($defaultCountryId) : $this->defaultCountry;
    }
}
