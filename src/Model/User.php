<?php

namespace src\Model;

use Propel\Runtime\ActiveQuery\Criteria;
use src\Model\Base\User as BaseUser;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Skeleton subclass for representing a row from the 'user' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class User extends BaseUser
{
    public function isEqualTo(UserInterface $user)
    {
        return true;
    }

    public function getTitle()
    {
        return $this->username;
    }

    /**
     * Возвращает удобочитаемый массив данных о пользователе
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getUserData()
    {
        $response = [];
        $response['username'] = $this->getUsername();
        $response['email'] = $this->getEmail();
        $photo = FileQuery::create()
            ->useUserQuery()
                ->filterByUsername($this->getUsername())
            ->endUse()
            ->orderById(Criteria::DESC)
            ->findOne();
        if ($photo) {
            $response['image'] = $photo->getUrl();
        }

        if ($birthday = $this->getBirthday()) {
            $response['birthday'] = $birthday->format('d-m-Y');
        }
        $response['reg_date'] = $this->getRegistrationDate();
        return $response;
    }
}
