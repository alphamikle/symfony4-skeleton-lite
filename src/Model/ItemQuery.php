<?php

namespace src\Model;

use src\Model\Base\ItemQuery as BaseItemQuery;
use Symfony\Component\Cache\Simple\FilesystemCache;

/**
 * Skeleton subclass for performing query and update operations on the 'item' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ItemQuery extends BaseItemQuery
{
    private $cache;
    public function __construct(string $dbName = 'default', string $modelName = '\\src\\Model\\Item', ?string $modelAlias = null)
    {
        $this->cache = new FilesystemCache();
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    public function getCache()
    {
        return $this->cache;
    }

    public function setQueryKey($key)
    {
        $this->queryKey = $key;
        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {
        $cache = $this->getCache();
        return $cache->has($key);
    }

    /**
     * @param $key
     * @return mixed|null
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function cacheFetch($key)
    {
        $cache = $this->getCache();
        return $cache->get($key);
    }

    /**
     * @param $key
     * @param $value
     * @param int $lifetime
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function cacheStore($key, $value, $lifetime = 600)
    {
        $cache = $this->getCache();
        $cache->set($key, $value, $lifetime);
    }
}
