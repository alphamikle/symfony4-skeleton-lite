<?php

namespace src\Model;

use Propel\Runtime\ActiveQuery\Criteria;
use src\Model\Base\DefaultCountryQuery as BaseDefaultCountryQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'default_country' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class DefaultCountryQuery extends BaseDefaultCountryQuery
{
    /**
     * @return array
     * @throws
     */
    public function getDefaultCountryOptions()
    {
        $selectedArray = CountryQuery::create()->select('Code')->find()->toArray();
        $rawArray = $this
            ->orderByTitle(Criteria::ASC)
            ->filterByCode($selectedArray, Criteria::NOT_IN)
            ->withColumn('Id', 'id')
            ->withColumn('Title', 'title')
            ->withColumn('Code', 'code')
            ->select(['id', 'title', 'code'])
            ->find()
            ->toArray();

        $preparedArray = array_map(function ($item) {
            $item['title'] = mb_strtolower($item['title']);
            $item['title'] = mb_strtoupper(mb_substr($item['title'], 0, 1)) . mb_substr($item['title'], 1, 100);
            $item['image'] = DefaultCountryQuery::create()->findPk($item['id'])->getFlag();
            return $item;
        }, $rawArray);

        return $preparedArray;
    }
}
