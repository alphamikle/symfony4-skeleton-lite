<?php

namespace App\Model;


use Symfony\Component\Security\Core\User\UserInterface;

abstract class UserEntity implements UserInterface
{
    abstract public function getPassword();
    abstract public function getRoles();
    abstract public function getUsername();
    public function getSalt()
    {
        return '';
    }
}