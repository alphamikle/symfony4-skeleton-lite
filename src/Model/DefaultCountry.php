<?php

namespace src\Model;

use src\Model\Base\DefaultCountry as BaseDefaultCountry;

/**
 * Skeleton subclass for representing a row from the 'default_country' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class DefaultCountry extends BaseDefaultCountry
{
    public function getFlag()
    {
        $code = $this->getCode();
        $filename = $code . '.png';
        $flagsDir = __DIR__ . '/../../public/images/flags/';
        if (is_file($flagsDir . $filename)) {
            return '/images/flags/' . $filename;
        } else {
            return '';
        }
    }
}
