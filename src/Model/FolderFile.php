<?php

namespace src\Model;

use src\Model\Base\FolderFile as BaseFolderFile;

/**
 * Skeleton subclass for representing a row from the 'folder_file' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class FolderFile extends BaseFolderFile
{

}
