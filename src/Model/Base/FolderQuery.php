<?php

namespace src\Model\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use src\Model\Folder as ChildFolder;
use src\Model\FolderQuery as ChildFolderQuery;
use src\Model\Map\FolderTableMap;

/**
 * Base class that represents a query for the 'folder' table.
 *
 *
 *
 * @method     ChildFolderQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildFolderQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildFolderQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method     ChildFolderQuery orderById($order = Criteria::ASC) Order by the id column
 *
 * @method     ChildFolderQuery groupByTitle() Group by the title column
 * @method     ChildFolderQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildFolderQuery groupByUpdatedAt() Group by the updated_at column
 * @method     ChildFolderQuery groupById() Group by the id column
 *
 * @method     ChildFolderQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildFolderQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildFolderQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildFolderQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildFolderQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildFolderQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildFolderQuery leftJoinUserRelatedByGalleryId($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserRelatedByGalleryId relation
 * @method     ChildFolderQuery rightJoinUserRelatedByGalleryId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserRelatedByGalleryId relation
 * @method     ChildFolderQuery innerJoinUserRelatedByGalleryId($relationAlias = null) Adds a INNER JOIN clause to the query using the UserRelatedByGalleryId relation
 *
 * @method     ChildFolderQuery joinWithUserRelatedByGalleryId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserRelatedByGalleryId relation
 *
 * @method     ChildFolderQuery leftJoinWithUserRelatedByGalleryId() Adds a LEFT JOIN clause and with to the query using the UserRelatedByGalleryId relation
 * @method     ChildFolderQuery rightJoinWithUserRelatedByGalleryId() Adds a RIGHT JOIN clause and with to the query using the UserRelatedByGalleryId relation
 * @method     ChildFolderQuery innerJoinWithUserRelatedByGalleryId() Adds a INNER JOIN clause and with to the query using the UserRelatedByGalleryId relation
 *
 * @method     ChildFolderQuery leftJoinUserRelatedByBackgroundGalleryId($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserRelatedByBackgroundGalleryId relation
 * @method     ChildFolderQuery rightJoinUserRelatedByBackgroundGalleryId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserRelatedByBackgroundGalleryId relation
 * @method     ChildFolderQuery innerJoinUserRelatedByBackgroundGalleryId($relationAlias = null) Adds a INNER JOIN clause to the query using the UserRelatedByBackgroundGalleryId relation
 *
 * @method     ChildFolderQuery joinWithUserRelatedByBackgroundGalleryId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserRelatedByBackgroundGalleryId relation
 *
 * @method     ChildFolderQuery leftJoinWithUserRelatedByBackgroundGalleryId() Adds a LEFT JOIN clause and with to the query using the UserRelatedByBackgroundGalleryId relation
 * @method     ChildFolderQuery rightJoinWithUserRelatedByBackgroundGalleryId() Adds a RIGHT JOIN clause and with to the query using the UserRelatedByBackgroundGalleryId relation
 * @method     ChildFolderQuery innerJoinWithUserRelatedByBackgroundGalleryId() Adds a INNER JOIN clause and with to the query using the UserRelatedByBackgroundGalleryId relation
 *
 * @method     ChildFolderQuery leftJoinFolderFile($relationAlias = null) Adds a LEFT JOIN clause to the query using the FolderFile relation
 * @method     ChildFolderQuery rightJoinFolderFile($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FolderFile relation
 * @method     ChildFolderQuery innerJoinFolderFile($relationAlias = null) Adds a INNER JOIN clause to the query using the FolderFile relation
 *
 * @method     ChildFolderQuery joinWithFolderFile($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FolderFile relation
 *
 * @method     ChildFolderQuery leftJoinWithFolderFile() Adds a LEFT JOIN clause and with to the query using the FolderFile relation
 * @method     ChildFolderQuery rightJoinWithFolderFile() Adds a RIGHT JOIN clause and with to the query using the FolderFile relation
 * @method     ChildFolderQuery innerJoinWithFolderFile() Adds a INNER JOIN clause and with to the query using the FolderFile relation
 *
 * @method     ChildFolderQuery leftJoinItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the Item relation
 * @method     ChildFolderQuery rightJoinItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Item relation
 * @method     ChildFolderQuery innerJoinItem($relationAlias = null) Adds a INNER JOIN clause to the query using the Item relation
 *
 * @method     ChildFolderQuery joinWithItem($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Item relation
 *
 * @method     ChildFolderQuery leftJoinWithItem() Adds a LEFT JOIN clause and with to the query using the Item relation
 * @method     ChildFolderQuery rightJoinWithItem() Adds a RIGHT JOIN clause and with to the query using the Item relation
 * @method     ChildFolderQuery innerJoinWithItem() Adds a INNER JOIN clause and with to the query using the Item relation
 *
 * @method     \src\Model\UserQuery|\src\Model\FolderFileQuery|\src\Model\ItemQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildFolder findOne(ConnectionInterface $con = null) Return the first ChildFolder matching the query
 * @method     ChildFolder findOneOrCreate(ConnectionInterface $con = null) Return the first ChildFolder matching the query, or a new ChildFolder object populated from the query conditions when no match is found
 *
 * @method     ChildFolder findOneByTitle(string $title) Return the first ChildFolder filtered by the title column
 * @method     ChildFolder findOneByCreatedAt(string $created_at) Return the first ChildFolder filtered by the created_at column
 * @method     ChildFolder findOneByUpdatedAt(string $updated_at) Return the first ChildFolder filtered by the updated_at column
 * @method     ChildFolder findOneById(int $id) Return the first ChildFolder filtered by the id column *

 * @method     ChildFolder requirePk($key, ConnectionInterface $con = null) Return the ChildFolder by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFolder requireOne(ConnectionInterface $con = null) Return the first ChildFolder matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFolder requireOneByTitle(string $title) Return the first ChildFolder filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFolder requireOneByCreatedAt(string $created_at) Return the first ChildFolder filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFolder requireOneByUpdatedAt(string $updated_at) Return the first ChildFolder filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFolder requireOneById(int $id) Return the first ChildFolder filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFolder[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildFolder objects based on current ModelCriteria
 * @method     ChildFolder[]|ObjectCollection findByTitle(string $title) Return ChildFolder objects filtered by the title column
 * @method     ChildFolder[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildFolder objects filtered by the created_at column
 * @method     ChildFolder[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildFolder objects filtered by the updated_at column
 * @method     ChildFolder[]|ObjectCollection findById(int $id) Return ChildFolder objects filtered by the id column
 * @method     ChildFolder[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class FolderQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \src\Model\Base\FolderQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\src\\Model\\Folder', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildFolderQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildFolderQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildFolderQuery) {
            return $criteria;
        }
        $query = new ChildFolderQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildFolder|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FolderTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = FolderTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFolder A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `title`, `created_at`, `updated_at`, `id` FROM `folder` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildFolder $obj */
            $obj = new ChildFolder();
            $obj->hydrate($row);
            FolderTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildFolder|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildFolderQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FolderTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildFolderQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FolderTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFolderQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FolderTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFolderQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(FolderTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(FolderTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FolderTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFolderQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(FolderTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(FolderTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FolderTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFolderQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FolderTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FolderTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FolderTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query by a related \src\Model\User object
     *
     * @param \src\Model\User|ObjectCollection $user the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFolderQuery The current query, for fluid interface
     */
    public function filterByUserRelatedByGalleryId($user, $comparison = null)
    {
        if ($user instanceof \src\Model\User) {
            return $this
                ->addUsingAlias(FolderTableMap::COL_ID, $user->getGalleryId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            return $this
                ->useUserRelatedByGalleryIdQuery()
                ->filterByPrimaryKeys($user->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserRelatedByGalleryId() only accepts arguments of type \src\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserRelatedByGalleryId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFolderQuery The current query, for fluid interface
     */
    public function joinUserRelatedByGalleryId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserRelatedByGalleryId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserRelatedByGalleryId');
        }

        return $this;
    }

    /**
     * Use the UserRelatedByGalleryId relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \src\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserRelatedByGalleryIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUserRelatedByGalleryId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserRelatedByGalleryId', '\src\Model\UserQuery');
    }

    /**
     * Filter the query by a related \src\Model\User object
     *
     * @param \src\Model\User|ObjectCollection $user the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFolderQuery The current query, for fluid interface
     */
    public function filterByUserRelatedByBackgroundGalleryId($user, $comparison = null)
    {
        if ($user instanceof \src\Model\User) {
            return $this
                ->addUsingAlias(FolderTableMap::COL_ID, $user->getBackgroundGalleryId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            return $this
                ->useUserRelatedByBackgroundGalleryIdQuery()
                ->filterByPrimaryKeys($user->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserRelatedByBackgroundGalleryId() only accepts arguments of type \src\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserRelatedByBackgroundGalleryId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFolderQuery The current query, for fluid interface
     */
    public function joinUserRelatedByBackgroundGalleryId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserRelatedByBackgroundGalleryId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserRelatedByBackgroundGalleryId');
        }

        return $this;
    }

    /**
     * Use the UserRelatedByBackgroundGalleryId relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \src\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserRelatedByBackgroundGalleryIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUserRelatedByBackgroundGalleryId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserRelatedByBackgroundGalleryId', '\src\Model\UserQuery');
    }

    /**
     * Filter the query by a related \src\Model\FolderFile object
     *
     * @param \src\Model\FolderFile|ObjectCollection $folderFile the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFolderQuery The current query, for fluid interface
     */
    public function filterByFolderFile($folderFile, $comparison = null)
    {
        if ($folderFile instanceof \src\Model\FolderFile) {
            return $this
                ->addUsingAlias(FolderTableMap::COL_ID, $folderFile->getFolderId(), $comparison);
        } elseif ($folderFile instanceof ObjectCollection) {
            return $this
                ->useFolderFileQuery()
                ->filterByPrimaryKeys($folderFile->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFolderFile() only accepts arguments of type \src\Model\FolderFile or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FolderFile relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFolderQuery The current query, for fluid interface
     */
    public function joinFolderFile($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FolderFile');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FolderFile');
        }

        return $this;
    }

    /**
     * Use the FolderFile relation FolderFile object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \src\Model\FolderFileQuery A secondary query class using the current class as primary query
     */
    public function useFolderFileQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFolderFile($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FolderFile', '\src\Model\FolderFileQuery');
    }

    /**
     * Filter the query by a related \src\Model\Item object
     *
     * @param \src\Model\Item|ObjectCollection $item the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFolderQuery The current query, for fluid interface
     */
    public function filterByItem($item, $comparison = null)
    {
        if ($item instanceof \src\Model\Item) {
            return $this
                ->addUsingAlias(FolderTableMap::COL_ID, $item->getGalleryId(), $comparison);
        } elseif ($item instanceof ObjectCollection) {
            return $this
                ->useItemQuery()
                ->filterByPrimaryKeys($item->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByItem() only accepts arguments of type \src\Model\Item or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Item relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFolderQuery The current query, for fluid interface
     */
    public function joinItem($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Item');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Item');
        }

        return $this;
    }

    /**
     * Use the Item relation Item object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \src\Model\ItemQuery A secondary query class using the current class as primary query
     */
    public function useItemQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Item', '\src\Model\ItemQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildFolder $folder Object to remove from the list of results
     *
     * @return $this|ChildFolderQuery The current query, for fluid interface
     */
    public function prune($folder = null)
    {
        if ($folder) {
            $this->addUsingAlias(FolderTableMap::COL_ID, $folder->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the folder table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FolderTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            FolderTableMap::clearInstancePool();
            FolderTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FolderTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(FolderTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            FolderTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            FolderTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildFolderQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(FolderTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildFolderQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(FolderTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildFolderQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(FolderTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildFolderQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(FolderTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildFolderQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(FolderTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildFolderQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(FolderTableMap::COL_CREATED_AT);
    }

} // FolderQuery
