<?php

namespace App\Command;

use src\Model\CountryQuery;
use src\Model\DefaultCountryQuery;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class JoinCountriesCommand extends Command
{
    protected static $defaultName = 'join:countries';

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $currentCountries = CountryQuery::create()->find();
        $defaultCountries = DefaultCountryQuery::create()->find();

        foreach ($currentCountries as $country) {
            $maxSimilar = 0;
            $curCountry = null;
            foreach ($defaultCountries as $defaultCountry) {
                similar_text(mb_strtolower($country->getTitle()), mb_strtolower($defaultCountry->getTitle()), $percent);
                if ($percent > $maxSimilar && $percent > 95) {
                    $maxSimilar = $percent;
                    $curCountry = $defaultCountry;
                }
            }
            if ($maxSimilar > 95) {
                $output->writeln($country->getTitle() . ' ' . $curCountry->getTitle() . ' ' . $maxSimilar);
                $country->setCode($curCountry->getCode())->save();
            }
        }
    }
}
