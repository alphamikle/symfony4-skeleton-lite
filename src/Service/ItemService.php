<?php

namespace App\Service;

use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Collection\ObjectCollection;
use src\Model\Category;
use src\Model\CategoryQuery;
use src\Model\Country;
use src\Model\CountryQuery;
use src\Model\Item;
use src\Model\ItemQuery;
use src\Model\Material;
use src\Model\MaterialQuery;
use src\Model\Subcategory;
use src\Model\SubcategoryQuery;
use src\Model\Subсategory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class ItemService
{

    private $container;
    private $cache;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->cache = $this->container->get('service.cache');
    }

    /**
     * Возвращает информацию об одном элементе в коллекции
     * @param $item Item
     * @return array
     * @throws
     */
    public function getItemData($item)
    {
        $cacheName = $this->cache::CACHE_ITEM_DATA . $item->getId();
        if ($this->cache->has($cacheName)) {
            return $this->cache->get($cacheName);
        }
        $output['id'] = $item->getId();
        $output['user'] = $item->getUser()->getUsername();
        $output['title'] = trim($item->getTitle());
        $output['price'] = $item->getPrice();
        $output['images'] = $item->getFiles();
        if ($item->getCountry()) {
            $output['country'] = $item->getCountry()->getTitle();
        }
        if ($item->getMaterial()) {
            $output['material'] = $item->getMaterial()->getTitle();
        }
        if ($item->getCategory()) {
            $output['category'] = $item->getCategory()->getTitle();
        }
        if ($item->getSubcategory()) {
            $output['subcategory'] = $item->getSubcategory()->getTitle();
        }
        $output['isExchange'] = $item->getIsExchange();
        $output['description'] = $item->getDescription();
        $output['createdAt'] = $item->getCreatedAt('d-m-Y H:i');

//        if ($this->cache->has($this->cache::COVER_IMAGE.))
        $cover = $item->getCover();
        $cover = preg_replace('/^\\//', '', $cover);
        $gregWar = $this->container->get('imager');
        $image = $gregWar->open($cover);
        $imageHeight = $image->height();
        $imageWidth = $image->width();
        if ($imageHeight > $imageWidth) {
            $cachedCover = $gregWar->open($cover)->cropResize(600, 600)->zoomCrop(450, 600, 0xFFFFFF, 0, 90)->jpeg(85);
            $output['isWide'] = false;
        } else {
            $cachedCover = $gregWar->open($cover)->cropResize(600, 600)->zoomCrop(600, 450, 0xFFFFFF, 120, 0)->jpeg(85);
            $output['isWide'] = true;
        }
        $output['cover'] = $cachedCover;
//        $output['cover'] = '';
        $this->cache->set($cacheName, $output);
        return $output;
    }

    /**
     * Возвращает инфу об итеме, пригодную для вставки в редактор
     * @param int $itemId
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getFullItemData($itemId)
    {
        $cacheName = $this->cache::CACHE_ITEM_FULL_DATA . $itemId;
        if ($this->cache->has($cacheName)) {
            return $this->cache->get($cacheName);
        }
        $item = ItemQuery::create()->findPk($itemId);
        $out['id'] = (int) $itemId;
        $out['title'] = $item->getTitle();
        $out['description'] = $item->getDescription();
        $out['price'] = (int) $item->getPrice();
        $out['isExchange'] = (bool) $item->getIsExchange();
        $out['category'] = (int) $item->getCategoryId();
        $out['subcategory'] = (int) $item->getSubcategoryId();
        $out['categoryTitle'] = $item->getCategory() ? $item->getCategory()->getTitle() : '';
        $out['subcategoryTitle'] = $item->getSubcategory() ? $item->getSubcategory()->getTitle() : '';
        $out['country'] = (int) $item->getCountryId();
        $out['countryTitle'] = $item->getCountry() ? $item->getCountry()->getTitle() : '';
        $out['material'] = (int) $item->getMaterialId();
        $out['materialTitle'] = $item->getMaterial() ? $item->getMaterial()->getTitle() : '';
        $out['userId'] = (int) $item->getUserId();
        $out['userName'] = $item->getUser()->getUsername();
        $out['galleryId'] = (int) $item->getGalleryId();

        $fileService = $this->container->get('service.file');
        if ($item->getGalleryId()) {
            $out['images'] = $fileService->getFolderData($item->getGalleryId());
        } else {
            $out['images'] = [];
        }
        $this->cache->set($cacheName, $out);
        return $out;
    }

    /**
     * Возвращает информацию об элементак в коллекции элементов
     * @param $itemQuery ItemQuery | ObjectCollection
     * @return array
     */
    public function getItemsData($itemQuery)
    {
        $output = [];
        foreach ($itemQuery as $item) {
            $output[] = $this->getItemData($item);
        }
        return $output;
    }

    /**
     * Возвращает список опций для селектов "аддера" элементов
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getAdderOptions()
    {
        $cacheName = $this->cache::CACHE_ADDER_OPTIONS;
//        if ($this->cache->has($cacheName)) {
//            return $this->cache->get($cacheName);
//        }
        $options = [];
        $options['countries'] = CountryQuery::create()->getCountryOptions();
        $options['materials'] = MaterialQuery::create()->getMaterialOptions();
        $options['categories'] = $this->getCategories();
        $options['subcategories'] = $this->getSubCategories();
        $this->cache->set($cacheName, $options);
        return $options;
    }

    /**
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getCategories()
    {
        $userProvider = $this->container->get('user_provider');
        $user = $userProvider->getCurrentUser();
        if ($user) {
            $visibles = json_decode($user->getVisibles(), true);
            if ($visibles && count($visibles)) {
                $nonVisibles = array_map(function ($mVisible) {
                    return (int)$mVisible['id'];
                }, array_filter($visibles, function ($fVisible) {
                    return $fVisible['visible'] === false;
                }));
                return CategoryQuery::create()->getCategoryOptions($nonVisibles);
            }
        }
        return CategoryQuery::create()->getCategoryOptions();
    }

    /**
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getSubCategories()
    {
        $userProvider = $this->container->get('user_provider');
        $user = $userProvider->getCurrentUser();
        if ($user) {
            $visibles = json_decode($user->getVisibles(), true);
            if ($visibles && count($visibles)) {
                $nonVisibles = array_map(function ($mVisible) {
                    return (int)$mVisible['id'];
                }, array_filter($visibles, function ($fVisible) {
                    return $fVisible['visible'] === false;
                }));
                return SubcategoryQuery::create()->getSubCategoryOptions($nonVisibles);
            }
        }
        return SubcategoryQuery::create()->getSubCategoryOptions();
    }

    /**
     * Возвращает массив Items в соответствии с request
     * @param Request $request
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getItemsAccordingRequest(Request $request)
    {
        $countries = $request->query->get('countries');
        $materials = $request->query->get('materials');
        $categories = $request->query->get('categories');
        $subcategories = $request->query->get('subcategories');
        $dates = $request->query->get('dates');
        $orderByTitle = $request->query->get('orderByTitle');
        $orderByDate = $request->query->get('orderByDate');
        $search = $request->query->get('search');
        $isExchange = $request->query->get('isExchange');
        $isMine = $request->query->get('isMine');
        $showAll = $request->query->get('showAll');
        $perPage = $request->query->get('perPage');
        $curPage = $request->query->get('curPage');
        $userId = $request->query->get('userId');

        if (!$curPage) {
            $curPage = 1;
        }

        if (!$perPage) {
            $perPage = 50;
        }

        $userProvider = $this->container->get('user_provider');
        $user = $userProvider->getCurrentUser();
        $itemService = $this->container->get('service.item');
        $itemsQuery = ItemQuery::create();


        if ($countries && count($countries)) {
            $itemsQuery = $itemsQuery->filterByCountryId($countries, Criteria::IN);
        }
        if ($materials && count($materials)) {
            $itemsQuery = $itemsQuery->filterByMaterialId($materials, Criteria::IN);
        }
        if ($categories && count($categories)) {
            $itemsQuery = $itemsQuery->filterByCategoryId($categories, Criteria::IN);
        }
        if ($subcategories && count($subcategories)) {
            $itemsQuery = $itemsQuery->filterBySubcategoryId($subcategories, Criteria::IN);
        }
        if ($user) {
            $visibles = json_decode($user->getVisibles(), true);
            if ($visibles && count($visibles)) {
                $nonVisibles = array_map(function ($mVisible) {
                    return (int)$mVisible['id'];
                }, array_filter($visibles, function ($fVisible) {
                    return $fVisible['visible'] === false;
                }));
                $itemsQuery = $itemsQuery->filterByCategoryId($nonVisibles, Criteria::NOT_IN);
            }
        }
        if ($dates && count($dates)) {
            $dateFrom = new \DateTime($dates[0]);
            $dateTo = new \DateTime($dates[1]);
            $itemsQuery = $itemsQuery->filterByCreatedAt(['min' => $dateFrom, 'max' => $dateTo]);
        }
        if ($orderByTitle) {
            $itemsQuery = $itemsQuery->orderByTitle($orderByTitle === 'ASC' ? Criteria::ASC : Criteria::DESC);
        }
        if ($orderByDate) {
            $itemsQuery = $itemsQuery->orderByCreatedAt($orderByDate === 'ASC' ? Criteria::ASC : Criteria::DESC);
        }
        if ($search) {
            $itemsQuery = $itemsQuery
                ->condition('C1', 'Item.Title LIKE ?', '%' . $search . '%')
                ->condition('C2', 'Item.Description LIKE ?', '%' . $search . '%')
                ->where(['C1', 'C2'], 'OR');
        }
        if ($isExchange) {
            $itemsQuery = $itemsQuery->filterByIsExchange(true);
        }
        if ($isMine && $userProvider->isAuth()) {
            $itemsQuery = $itemsQuery->filterByUser($user);
        }
        if ($userId && $userProvider->isAuth()) {
            $itemsQuery = $itemsQuery->filterByUserId($userId);
        }
        if (!$orderByTitle && !$orderByDate) {
            $itemsQuery = $itemsQuery->orderByCreatedAt(Criteria::DESC);
        }

        $itemCount = $itemsQuery->count();
        if (!$showAll) {
            $itemsQuery = $itemsQuery->paginate($curPage, $perPage);
            $data['pagination']['last'] = $itemsQuery->getLastPage();
            $data['pagination']['prev'] = $itemsQuery->getPreviousPage();
            $data['pagination']['next'] = $itemsQuery->getNextPage();
            if ($curPage > $data['pagination']['last']) {
                $curPage = $data['pagination']['last'];
            }
            $data['pagination']['current'] = (int) $curPage ;
            $data['pagination']['links'] = $itemsQuery->getLinks(6);
            $data['pagination']['count'] = $itemCount;
            $data['pagination']['curPage'] = $curPage;
            $data['pagination']['perPage'] = $perPage;
        } else {
            $itemsQuery = $itemsQuery->find();
        }
        $data['itemsList'] = $itemService->getItemsData($itemsQuery);

        return $data;
    }

    public function validateNewOptions($fields)
    {
        $errors = [];
        foreach ($fields as $field => $value) {
            switch ($field) {
                case 'country': {
                    if ($value && CountryQuery::create()->issetCountry($value)) {
                        $errors['errors'][$field] = 'Страна уже выбрана';
                    }
                    break;
                }
                case 'category': {
                    $value = trim($value);
                    if ($value && CategoryQuery::create()->filterByTitle($value)->count()) {
                        $errors['errors'][$field] = 'Категория уже выбрана';
                    }
                    break;
                }
                case 'subcategorycategory': {
                    $value = trim($value);
                    if ($value && SubcategoryQuery::create()->filterByTitle($value)->count()) {
                        $errors['errors'][$field] = 'Подкатегория уже выбрана';
                    }
                    break;
                }
                case 'material': {
                    $value = trim($value);
                    if ($value && MaterialQuery::create()->filterByTitle($value)->count()) {
                        $errors['errors'][$field] = 'Материал уже выбран';
                    }
                    break;
                }
            }
        }
        return $errors;
    }

    public function createNewOptions($fields)
    {
        foreach ($fields as $field => $value) {
            if ($value) {
                switch ($field) {
                    case 'country':
                        {
                            $newCountry = new Country();
                            $newCountry->copyDefaultCountry($value)->save();
                            break;
                        }
                    case 'category':
                        {
                            $value = trim($value);
                            $newCategory = new Category();
                            $newCategory->setTitle($value)->save();
                            break;
                        }
                    case 'subcategory':
                        {
                            $value = trim($value);
                            $newSubCategory = new Subcategory();
                            $newSubCategory->setTitle($value)->save();
                            break;
                        }
                    case 'material':
                        {
                            $value = trim($value);
                            $newMaterial = new Material();
                            $newMaterial->setTitle($value)->setDescription('')->save();
                            break;
                        }
                }
            }
        }
    }
}
