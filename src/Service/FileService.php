<?php

namespace App\Service;

use Propel\Runtime\ActiveQuery\Criteria;
use Psr\Container\ContainerInterface;
use src\Model\File;
use src\Model\FileQuery;
use src\Model\Folder;
use src\Model\FolderFile;
use src\Model\FolderFileQuery;
use src\Model\FolderQuery;
use src\Model\ItemQuery;
use src\Model\UserQuery;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class FileService
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param Request $request
     * @param $folderId int
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function uploadFiles(Request $request, int $folderId) : array
    {
        $files = [];
        $folder = FolderQuery::create()->findPk($folderId);
        if (UserQuery::create()->filterByGalleryId($folderId)->count()) {
            FolderFileQuery::create()->filterByFolderId($folderId)->delete();
        }
        foreach ($request->files as $uploadedFile) {
            if ($uploadedFile instanceof UploadedFile) {
                $generatedName = md5($uploadedFile->getClientOriginalName() . time()) . '.' . $uploadedFile->getClientOriginalExtension();
                $path = $this->container->get('kernel')->getRootDir() . '/../public/uploads';
                if (!is_dir($path)) {
                    mkdir($path);
                }
                $file = [];
                $file['title'] = $uploadedFile->getClientOriginalName();
                $file['url'] = '/uploads/' . $generatedName;
                $file['mime'] = $uploadedFile->getClientMimeType();
                $file['size'] = $uploadedFile->getSize();
                $uploadedFile->move($path, $generatedName);
                $files[] = $this->setFileToFolder($file, $folder);
            }
        }
        return $files;
    }

    /**
     * @param $file array
     * @param $folder Folder
     * @throws \Propel\Runtime\Exception\PropelException
     * @return array
     */
    public function setFileToFolder($file, $folder)
    {
        $fileEntity = new File();
        $fileEntity
            ->setTitle($file['title'])
            ->setUrl($file['url'])
            ->setMime($file['mime'])
            ->setSize($file['size'])
            ->save();

        $folderFile = new FolderFile();
        $folderFile
            ->setFileId($fileEntity->getId())
            ->setFolderId($folder->getId())
            ->save();

        $file['id'] = $fileEntity->getId();
        $file['folder_id'] = $folder->getId();
        return $file;
    }

    /**
     * Все это - ебучий говнокод, когда-нибудь я обязательно перепишу его
     * в нормальный вид, возможно не на этом сайте, но свой парсер я напишу отлично!
     * @param null $userId
     * @return Folder
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function createFolder($userId = null)
    {
        $folder = new Folder();
        $folder
            ->setTitle((new \DateTime('now'))->format('d-m-Y H:i:s') . '_' . md5(microtime()))
            ->save();
        if ($userId !== null) {
            UserQuery::create()->findPk($userId)->setGalleryId($folder->getId())->save();
        }
        return $folder;
    }

    /**
     * Производит удаление файла
     * @param $file File
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function deleteFile($file)
    {
        $fullPath = $this->getFullPath($file);
        if (is_file($fullPath)) {
            unlink($fullPath);
        }
        $file->delete();
    }

    /**
     * Возвращает инфу об файле
     * @param int $fileId
     * @return array
     */
    public function getFileData($fileId)
    {
        $gregWar = $this->container->get('imager');
        $file = FileQuery::create()->findPk($fileId);
        $width = $gregWar->open($file->getRelativeUrl())->width();
        $height = $gregWar->open($file->getRelativeUrl())->height();
        $wH = $width / $height;
        $baseWidth = 650;

        $out['folder_id'] = FolderFileQuery::create()->filterByFileId($fileId)->findOne()->getFolderId();
        $out['id'] = $fileId;
        $out['mime'] = $file->getMime();
        $out['previewUrl'] = '/' . $gregWar->open($file->getRelativeUrl())->cropResize($baseWidth,$baseWidth * $wH)->jpeg(90);
        $out['url'] = $file->getUrl();
        $out['size'] = (int) $file->getSize();
        $out['title'] = $file->getTitle();
        $out['name'] = $file->getTitle();
        $out['width'] = $width;
        $out['height'] = $height;
        return $out;
    }

    /**
     * Возвращает инфу о файлах для редактора в соответствии с папкой,
     * к которой они прикреплены
     * @param int $folderId
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getFolderData($folderId)
    {
        $fileIds = FolderFileQuery::create()->filterByFolderId($folderId)->select(['FileId'])->find()->toArray();
        $out = [];
        foreach ($fileIds as $fileId) {
            $out[] = $this->getFileData($fileId);
        }
        return $out;
    }

    /**
     * Возвращает полный путь до файла
     * @param $file File
     * @return string
     * @throws
     */
    public function getFullPath($file)
    {
        return preg_replace('/\\\src$/', '/public', $this->container->get('kernel')->getRootDir()) . $file->getUrl();
    }

    /**
     * Удаляет лишние файлы, которые были откреплены
     * от предмета при сохранении
     * @param $rightFilesIds array
     * @param $folder Folder
     * @throws
     */
    public function deleteExcessFiles($folder, $rightFilesIds)
    {
        if (!empty($rightFilesIds)) {
            $excessFiles = FileQuery::create()
                ->useFolderFileQuery()
                ->filterByFolderId($folder->getId())
                ->endUse()
                ->filterById($rightFilesIds, Criteria::NOT_IN)
                ->find();

            if (count($excessFiles)) {
                foreach ($excessFiles as $excessFile) {
                    $this->deleteFile($excessFile);
                }
            }
        }
    }
}
