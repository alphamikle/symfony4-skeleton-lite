<?php
namespace App\Service;

use RedisException;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\Cache\Simple\RedisCache;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CacheService
{
    const CACHE_ITEM_DATA = 'ITEM';
    const CACHE_ITEMS_DATA = 'ITEMS';
    const CACHE_ITEM_FULL_DATA = 'ITEM_FULL';
    const CACHE_ADDER_OPTIONS = 'OPTIONS';
    const COVER_IMAGE = 'COVER';

    private $cache;
    private $container;
    public function __construct(ContainerInterface $container)
    {
        try {
            $redisClient = RedisAdapter::createConnection('redis://localhost');
        } catch (RedisException $e) {
            $redisClient = null;
        }
        if ($redisClient !== null) {
            $this->cache = new RedisCache($redisClient);
        } else {
            $this->cache = new FilesystemCache();
        }
        $this->container = $container;
    }

    private function getCacheContainer()
    {
        return $this->cache;
    }

    /**
     * @param $key
     * @param $value
     * @param float|int $ttl
     * @return void
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function set($key, $value, $ttl = (60 * 60 * 24 * 365))
    {
//        if ($this->container->get('kernel')->getEnvironment() === 'dev') {
//            return false;
//        }
        $this->getCacheContainer()->set($key, $value, $ttl);
    }

    /**
     * @param $key
     * @return mixed|null
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function get($key)
    {
        return $this->getCacheContainer()->get($key);
    }

    public function has($key)
    {
//        if ($this->container->get('kernel')->getEnvironment() === 'dev') {
//            return false;
//        }
        return $this->getCacheContainer()->has($key);
    }

    public function delete($key)
    {
        $this->getCacheContainer()->delete($key);
    }

    public function clear()
    {
        $this->getCacheContainer()->clear();
    }
}