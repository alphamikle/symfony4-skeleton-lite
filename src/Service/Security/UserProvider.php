<?php

namespace App\Service\Security;

use Psr\Container\ContainerInterface;
use src\Model\UserQuery;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface
{
    private $container;
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function loadUserByUsername($username = '')
    {
        if (empty($username)) {
            throw new UsernameNotFoundException('Имя пользователя не задано');
        }

        $userEntity = UserQuery::create()->filterByEmail($username)->findOne();

        if (!$userEntity) {
            throw new UsernameNotFoundException('Пользователь с таким логином не найден');
        }
        return $userEntity;
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException('Проблемы с сущностью юзера???');
        }
        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'App\\Model\\User';
    }

    /**
     * TODO: Разобраться, как возвращать сущность дефолтными методами Symfony
     * Возвращает сущность пользователя (костылиииии)
     * @return null|\src\Model\User|UserInterface
     */
    public function getCurrentUser()
    {
        $userCore = $this->container->get('security.token_storage')->getToken()->getUser();
        if ($userCore != 'anon.') {
            try {
                return $this->loadUserByUsername($userCore);
            } catch (UsernameNotFoundException $exception) {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Авторизован ли пользователь
     * @return bool
     */
    public function isAuth()
    {
        return $this->container->get('security.token_storage')->getToken()->getUser() != 'anon.';
    }

    /**
     * Возвращает кастомный токен для проверки подлинности User'a
     * @return null|string
     */
    public function getCurrentUserToken()
    {
        $userCore = new User();
        $currentUser = $this->getCurrentUser();
        if ($currentUser == null) {
            return null;
        } else {
            $date = new \DateTime();
            $date = $date->format('m');
            $currentUserTokenData = $currentUser->getId() . $currentUser->getEmail() . $currentUser->getPassword() . $date;
            $currentUserToken = $this->container->get('security.password_encoder')->encodePassword($userCore, $currentUserTokenData);
            return $currentUserToken;
        }
    }

    /**
     * Проверяет, соответствует ли токен User'a токену подлинного User'a
     * @param $token
     * @return bool
     */
    public function isCurrentUserTrusted($token)
    {
        $currentUserToken = $this->getCurrentUserToken();
        if ($currentUserToken == $token) {
            return true;
        }
        return false;
    }

}