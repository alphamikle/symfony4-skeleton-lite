<?php
namespace App\Service\Security;

use Psr\Container\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserAuthorizator
{
    private $container;
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function authorizeUser(\src\Model\User $user)
    {
        $token = new UsernamePasswordToken($user->getEmail(), null, 'main', ['ROLE_USER']);
        $this->container->get('security.token_storage')->setToken($token);
        $this->container->get('session')->set('_security_main', serialize($token));
    }
}