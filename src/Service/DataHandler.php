<?php
namespace App\Service;

use Psr\Container\ContainerInterface;

class DataHandler
{
    private $container;
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Возвращает "везде-используемую" информацию
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getCommonData()
    {
        $response = [];

        $userProvider = $this->container->get('user_provider');
        $currentUser = $userProvider->getCurrentUser();

        $itemService = $this->container->get('service.item');
        $response['options'] = $itemService->getAdderOptions();
        if ($currentUser != null) {


            $response['user_data'] = $currentUser->getUserData();
            $response['is_authorized'] = true;
            $response['user_token'] = $userProvider->getCurrentUserToken();

        } else {

            $response['is_authorized'] = false;

        }

        return json_encode($response);
    }
}